package com.nedash.com.wifi.analyzer.ping.tool.signal.utils

import com.android.billingclient.api.SkuDetails
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

object AdUtils {
    fun SkuDetails.convertPrice(): String {
        val price = priceAmountMicros / 1000000.0
        return getText(price.toString())
    }

    fun SkuDetails.calculateOldPrice(discount: Int): String {
        with(DecimalFormat("#.##")) {
            decimalFormatSymbols = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            val price =
                format(priceAmountMicros / 1000000.0 * 100 / (100 - discount))
            return getText(price)
        }
    }

    fun SkuDetails.calculatePeriodPrice(period: Int): String {
        with(DecimalFormat("#.##")) {
            decimalFormatSymbols = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            val price = format(priceAmountMicros / 1000000.0 / period)
            return getText(price)
        }
    }

    private fun SkuDetails.getText(price: String): String =
        if (priceCurrencyCode.length > 1) "$price $priceCurrencyCode" else priceCurrencyCode + price
}

open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}