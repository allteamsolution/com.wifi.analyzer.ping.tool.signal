package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentHomeBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.ui.adapter.HomeItemsAdapter
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.Utils.navigateWithSlideAnimation
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.item_adapter.HomeItem


class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val homeItemsAdapter = HomeItemsAdapter(onItemClick = { homeItem ->
        when(homeItem){
            HomeItem.IpInformation -> homeItem.navId
            HomeItem.Whois -> homeItem.navId
            HomeItem.Ping -> homeItem.navId
            HomeItem.Traceroute -> homeItem.navId
            HomeItem.PortScanner -> homeItem.navId
            HomeItem.WifiExplorer -> homeItem.navId
            HomeItem.LanScanner -> homeItem.navId
            HomeItem.DnsLookup -> homeItem.navId
            HomeItem.IpHostConverter -> homeItem.navId
            else -> 0
        }.let { navId ->
            if (navId != 0)
                findNavController().navigateWithSlideAnimation(navId)
        }
    })

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding.rvItems) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = homeItemsAdapter
        }
        homeItemsAdapter.submitList(HomeItem.getAllItems())
    }
}