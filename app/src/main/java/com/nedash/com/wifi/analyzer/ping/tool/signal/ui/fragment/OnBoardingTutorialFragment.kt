package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.nedash.com.wifi.analyzer.ping.tool.signal.R
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentOnBoardingTutorialBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentOnBoardingViewPagerContentBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.ui.activity.MainActivity


class OnBoardingTutorialFragment : Fragment() {
    private lateinit var binding: FragmentOnBoardingTutorialBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOnBoardingTutorialBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pagerAdapter = OnBoardingSlidePagerAdapter(this)
        with(binding) {
            introPager.adapter = pagerAdapter
            TabLayoutMediator(intoTabLayout, introPager)
            { _, _ -> }.attach()
            introPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    tvNext.setText(
                        when (position) {
                            2 -> R.string.start
                            else -> R.string.next
                        }
                    )
                }
            })
            tvNext.setOnClickListener {
                when (introPager.currentItem) {
                    2 -> navNext()
                    else -> introPager.currentItem = introPager.currentItem + 1
                }
            }
            tvSkip.setOnClickListener { navNext() }
        }
    }

    private fun navNext() {
        findNavController().navigate(
            if ((requireActivity() as MainActivity).isPro)
                OnBoardingTutorialFragmentDirections
                    .actionOnBoardingTutorialFragmentToHomeFragment()
            else OnBoardingTutorialFragmentDirections
                .actionOnBoardingTutorialFragmentToPurchaseAfterTutorialFragment()
        )
    }

    inner class OnBoardingSlidePagerAdapter(
        fragment: Fragment,
    ) : FragmentStateAdapter(fragment) {

        override fun getItemCount() = 3

        override fun createFragment(position: Int): Fragment {
            return OnBoardingViewPagerContentFragment().apply {
                arguments = Bundle().apply { putInt("position", position) }
            }
        }
    }
}

class OnBoardingViewPagerContentFragment : Fragment() {

    private lateinit var binding: FragmentOnBoardingViewPagerContentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            FragmentOnBoardingViewPagerContentBinding
                .inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            arguments?.takeIf { it.containsKey("position") }?.apply {
                when (getInt("position")) {
                    0 -> {
                        ivIcon.setImageResource(R.drawable.img_tuturial_1)
                        tvHint.setText(R.string.tutorial_text_1)
                    }
                    1 -> {
                        ivIcon.setImageResource(R.drawable.img_tuturial_2)
                        tvHint.setText(R.string.tutorial_text_2)
                    }
                    else -> {
                        ivIcon.setImageResource(R.drawable.img_tuturial_3)
                        tvHint.setText(R.string.tutorial_text_3)
                    }
                }
            }
        }
    }
}