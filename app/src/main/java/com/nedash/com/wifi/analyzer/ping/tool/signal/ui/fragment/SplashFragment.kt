package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentSplashBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.SharedPrefs

class SplashFragment : Fragment() {

    private lateinit var binding: FragmentSplashBinding
    private lateinit var sharedPrefs: SharedPrefs

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        sharedPrefs = SharedPrefs(requireContext())
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Handler(Looper.getMainLooper()).postDelayed({
            findNavController().navigate(
                if (sharedPrefs.getBool(SharedPrefs.TUTORIAL_SHOWED))
                    SplashFragmentDirections
                        .actionSplashFragmentToHomeFragment()
                else
                    SplashFragmentDirections
                        .actionSplashFragmentToPurchaseBeforeTutorialFragment()
            )
        }, 1000)
    }
}