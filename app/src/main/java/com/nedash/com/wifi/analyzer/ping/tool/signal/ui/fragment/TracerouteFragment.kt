package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentPortScannerBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentTracerouteBinding

class TracerouteFragment : Fragment() {
    private lateinit var binding: FragmentTracerouteBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTracerouteBinding.inflate(layoutInflater, container, false)
        return binding.root
    }
}