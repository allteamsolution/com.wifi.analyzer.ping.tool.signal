package com.nedash.com.wifi.analyzer.ping.tool.signal.utils

object Constants {
    object AdConstants {


        const val SITE_URL = ""
        const val PP_URL = ""
        const val TOU_URL = ""
        const val OUR_APPS_URL = ""
        const val PURCHASE_CANCEL_SITE = "https://support.google.com/googlenews/answer/7018481?co=GENIE.Platform%3DAndroid&hl="

        const val SUBS_YEAR_BEFORE = "subscribe_wifi_analyzer_ping_trial_before"
        const val SUBS_YEAR_AFTER = "subscribe_wifi_analyzer_ping_trial_after"
        const val SUBS_OFFER = "subscribe_wifi_analyzer_ping_offer"
        const val SUBS_INSIDE = "subscribe_wifi_analyzer_ping_inside_trial"

        const val SUBS_WEEK = "subscribe_wifi_analyzer_ping_week"
        const val SUBS_MONTH = "subscribe_wifi_analyzer_ping_month"
        const val SUBS_YEAR = "subscribe_wifi_analyzer_ping_year"







    }
}