package com.nedash.com.wifi.analyzer.ping.tool.signal.utils.item_adapter

import androidx.annotation.DrawableRes
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import com.nedash.com.wifi.analyzer.ping.tool.signal.R

sealed class HomeItem constructor(
    @DrawableRes val icon: Int,
    @StringRes val header: Int,
    @DrawableRes val background: Int,
    @IdRes val navId: Int){

    object IpInformation: HomeItem(
        icon = R.drawable.ic_ip_information_with_bg,
        header = R.string.ip_info,
        background = R.drawable.ip_info_item_bg,
        navId = R.id.ipInfoFragment
    )
    object Whois: HomeItem(
        icon = R.drawable.ic_whois_with_bg,
        header = R.string.whois,
        background = R.drawable.whois_item_bg,
        navId = R.id.whoisFragment
    )
    object Ping: HomeItem(
        icon = R.drawable.ic_ping_with_bg,
        header = R.string.ping,
        background = R.drawable.ping_item_bg,
        navId = R.id.pingFragment
    )
    object Traceroute: HomeItem(
        icon = R.drawable.ic_traceroute_with_bg,
        header = R.string.traceroute,
        background = R.drawable.traceroute_item_bg,
        navId = R.id.pingFragment
    )
    object PortScanner: HomeItem(
        icon = R.drawable.ic_port_scanner_with_bg,
        header = R.string.port_scanner,
        background = R.drawable.port_scanner_item_bg,
        navId = R.id.pingFragment
    )
    object WifiExplorer: HomeItem(
        icon = R.drawable.ic_wifi_explorer_with_bg,
        header = R.string.wifi_explorer,
        background = R.drawable.wifi_explorer_item_bg,
        navId = R.id.wifiExplorerFragment
    )
    object LanScanner: HomeItem(
        icon = R.drawable.ic_lan_scanner_with_bg,
        header = R.string.lan_scanner,
        background = R.drawable.lan_scanner_item_bg,
        navId = R.id.lanScannerFragment
    )
    object DnsLookup: HomeItem(
        icon = R.drawable.ic_dns_lookup_with_bg,
        header = R.string.dns_lookup,
        background = R.drawable.dns_lookup_item_bg,
        navId = R.id.dnsLookupFragment
    )
    object IpHostCalculator: HomeItem(
        icon = R.drawable.ic_ip_calculator_with_bg,
        header = R.string.ip_calculator,
        background = R.drawable.ip_host_calculator_item_bg,
        navId = R.id.ipHostConverterFragment
    )
    object IpHostConverter: HomeItem(
        icon = R.drawable.ic_ip_host_converter_with_bg,
        header = R.string.ip_host_converter,
        background = R.drawable.ip_host_converter_item_bg,
        navId = R.id.ipHostConverterFragment
    )


    companion object{
        fun getAllItems() =  listOf(
            IpInformation,
            Whois,
            Ping,
            Traceroute,
            PortScanner,
            WifiExplorer,
            LanScanner,
            DnsLookup,
            IpHostCalculator,
            IpHostConverter
        )
    }
}