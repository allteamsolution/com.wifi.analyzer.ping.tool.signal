package com.nedash.com.wifi.analyzer.ping.tool.signal.utils

import android.content.Context

class SharedPrefs(context: Context) {
    companion object {
        const val LAST_OFFER_SHOW = "last_offer_show"
        const val TUTORIAL_SHOWED = "tutorial_showed"
        const val SHOW_NOTIFICATIONS = "show_notifications"
        const val OFFER_SHOW_COUNTER = "offer_show_counter"
    }

    private val sharedPreferences = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)

    fun putValue(key: String, value: Any) {
        with(sharedPreferences.edit()) {
            when (value) {
                is String -> putString(key, value)
                is Int -> putInt(key, value)
                is Long -> putLong(key, value)
                is Boolean -> putBoolean(key, value)
            }
            apply()
        }
    }

    fun getString(key: String) = sharedPreferences.getString(key, null)

    fun getInt(key: String) = sharedPreferences.getInt(key, 0)

    fun getLong(key: String) = sharedPreferences.getLong(key, 0L)

    fun getBool(key: String, default: Boolean = false) = sharedPreferences.getBoolean(key, default)
}
