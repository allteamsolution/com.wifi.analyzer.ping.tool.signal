package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentLanScannerBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.FragmentPingBinding

class PingFragment : Fragment() {
    private lateinit var binding: FragmentPingBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPingBinding.inflate(layoutInflater, container, false)
        return binding.root
    }
}