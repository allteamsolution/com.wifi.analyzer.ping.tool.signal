package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.ItemListHomeBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.item_adapter.HomeItem

class HomeItemsAdapter constructor(
    val onItemClick: (HomeItem) -> Unit
): ListAdapter<HomeItem, HomeItemsAdapter.HomeItemsViewHolder>(
    object : DiffUtil.ItemCallback<HomeItem>() {
        override fun areItemsTheSame(oldItem: HomeItem, newItem: HomeItem) =
            oldItem.header == newItem.header

        override fun areContentsTheSame(oldItem: HomeItem, newItem: HomeItem) =
            oldItem == newItem }){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeItemsViewHolder {
        val binding = ItemListHomeBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)

        return HomeItemsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeItemsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class HomeItemsViewHolder(private val binding: ItemListHomeBinding):
        RecyclerView.ViewHolder(binding.root){
            fun bind(homeItem: HomeItem){
                with(binding){
                    with(homeItem){
                        clContainer.setBackgroundResource(background)
                        ivIcon.setImageResource(icon)
                        tvHeader.setText(header)

                        root.setOnClickListener {
                            onItemClick(homeItem)
                        }
                    }
                }
            }
    }
}