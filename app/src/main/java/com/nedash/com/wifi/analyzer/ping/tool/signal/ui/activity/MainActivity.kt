package com.nedash.com.wifi.analyzer.ping.tool.signal.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.ads.*
import com.nedash.com.wifi.analyzer.ping.tool.signal.BuildConfig
import com.nedash.com.wifi.analyzer.ping.tool.signal.R
import com.nedash.com.wifi.analyzer.ping.tool.signal.billing.BillingHelper
import com.nedash.com.wifi.analyzer.ping.tool.signal.databinding.ActivityMainBinding
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.Utils.gone
import com.nedash.com.wifi.analyzer.ping.tool.signal.utils.Utils.visible

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var billingHelper: BillingHelper
    private lateinit var adView: AdView
    var isPro: Boolean = false


    private val adSize: AdSize
        get() {
            val display = windowManager.defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)

            val density = outMetrics.density

            var adWidthPixels = binding.contentContainer.adViewContainer.width.toFloat()
            if (adWidthPixels == 0f) {
                adWidthPixels = outMetrics.widthPixels.toFloat()
            }

            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        with(binding) {

            setSupportActionBar(contentContainer.toolbar)
            setContentView(root)

            val navController = findNavController(R.id.nav_host_fragment_content_main)
            appBarConfiguration = AppBarConfiguration(
                setOf(R.id.homeFragment), drawerLayout
            )

            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)
//            contentContainer.llGetPro.setOnClickListener { findNavController(R.id.nav_host_fragment_content_main).navToInside() }
//            TODO("ADD HERE FOR GET PRO BUTTON IN TOOLBAR ")
//            TODO("ADD HERE FUNCTIONALITY TO ITEMS IN DRAWER LAYOUT")
        }

        billingHelper = BillingHelper(this)

        with(billingHelper) {
            init()
            isPro.observe(this@MainActivity, {
                this@MainActivity.isPro = it ?: false
                with(binding) {
                    if (this@MainActivity.isPro) {
                        with(contentContainer) {
                            llGetPro.gone()
                            rlBanner.gone()
                        }
                    } else {
                        with(contentContainer) {
                            llGetPro.visible()
                            rlBanner.visible()
                        }
                    }
                }
            })
            skuQueryError.observe(this@MainActivity, {
                it?.getContentIfNotHandled().let { error ->
                    error?.let {
                        Toast.makeText(
                            this@MainActivity,
                            R.string.some_error_occurred,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            })
        }
        showBannerOrPro()
    }

    private val listener =
        NavController.OnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
//                TODO("ADD HERE LISTENER ON FRAGMENTS AND CHANGES TOOLBAR STATE HERE")
            }
        }

    private fun showOrHide(show: Boolean, @ColorRes color: Int, lock: Boolean = true, isLlGetPro: Boolean = true) {
        window?.statusBarColor = ContextCompat.getColor(this, color)
        with(binding) {
            with(contentContainer) {
                if (show) {
                    toolbar.visible()
                    if (!isPro){
                        rlBanner.visible()
                        toolbar.visible()
                        if(!isLlGetPro) {
                            contentContainer.llGetPro.gone()
                        } else{
                            contentContainer.llGetPro.visible()
                        }
                    }
                } else {
                    toolbar.gone()
                    rlBanner.gone()
                }
                drawerLayout.setDrawerLockMode(if (lock) DrawerLayout.LOCK_MODE_LOCKED_CLOSED else DrawerLayout.LOCK_MODE_UNLOCKED)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onResume() {
        super.onResume()
        findNavController(R.id.nav_host_fragment_content_main).addOnDestinationChangedListener(
            listener
        )
    }

    override fun onPause() {
        super.onPause()
        findNavController(R.id.nav_host_fragment_content_main)
            .removeOnDestinationChangedListener(listener)
    }

    override fun onDestroy() {
        super.onDestroy()
        billingHelper.onDestroy()
    }

    override fun onBackPressed() {
        binding.contentContainer.ivGetProBanner
        if (binding.drawerLayout.isOpen)
            binding.drawerLayout.close()
        else
            when (findNavController(R.id.nav_host_fragment_content_main).currentDestination?.id) {
                R.id.purchaseBeforeTutorialFragment,
                R.id.purchaseAfterTutorialFragment,
                R.id.purchaseOfferFragment -> return
                else -> super.onBackPressed()
            }
    }

    private fun showBannerOrPro() {
        adView = AdView(this)
        with(binding.contentContainer) {
            adViewContainer.addView(adView)

            adView.adUnitId = BuildConfig.BANNER_ADD

            adView.adSize = adSize

            val adRequest = AdRequest.Builder().build()

            // Start loading the ad in the background.
            adView.loadAd(adRequest)
            adView.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    ivGetProBanner.gone()
                    adViewContainer.visible()
                }

                override fun onAdFailedToLoad(p0: LoadAdError) {
                    super.onAdFailedToLoad(p0)
                    adViewContainer.gone()
                    ivGetProBanner.visible()
                }
            }
        }
    }
}