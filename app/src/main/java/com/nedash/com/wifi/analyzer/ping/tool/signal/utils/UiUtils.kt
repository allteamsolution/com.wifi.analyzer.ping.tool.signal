package com.nedash.com.wifi.analyzer.ping.tool.signal.utils

import com.nedash.com.wifi.analyzer.ping.tool.signal.R
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.nedash.com.wifi.analyzer.ping.tool.signal.BuildConfig
import java.util.*

object Utils {

    fun View.gone() {
        visibility = View.GONE
    }

    fun View.visible() {
        visibility = View.VISIBLE
    }

    fun View.invisible() {
        visibility = View.INVISIBLE
    }

    fun View.enable() {
        isEnabled = true
    }

    fun View.disable() {
        isEnabled = false
    }

    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun NavController.navigateWithSlideAnimation(@IdRes resId: Int) {
        val navOptions = NavOptions.Builder().apply {
            setEnterAnim(R.anim.slide_in_right)
            setExitAnim(R.anim.slide_out_left)
            setPopEnterAnim(R.anim.slide_in_left)
            setPopExitAnim(R.anim.slide_out_right)
        }
        navigate(resId, null, navOptions.build())
    }

    fun NavController.navToInsidePurchaseFragment() {
        val navOptions = NavOptions.Builder().apply {
            setEnterAnim(android.R.anim.fade_in)
            setExitAnim(android.R.anim.fade_out)
        }
        navigate(R.id.purchaseInsideApplicationFragment, null, navOptions.build())
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager =
            getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun Fragment.getColoredText(@StringRes id: Int, vararg args: Any?): CharSequence =
        HtmlCompat.fromHtml(String.format(getString(id), *args), HtmlCompat.FROM_HTML_MODE_COMPACT)

    fun Context.appInstalledOrNot(uri: String): Boolean {
        val pm = packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return false
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return true
    }

    fun Context.openURL(url: String) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(url)
            })
        } catch (e: Exception) {
            Toast.makeText(this, "R.string.no_application_found", Toast.LENGTH_SHORT).show()
        }
    }

    fun Context.shareApp() {
        try {
            startActivity(Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(
                    Intent.EXTRA_TEXT,
                    "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}"
                )
            })
        } catch (e: Exception) {
            Toast.makeText(this, "R.string.no_application_found", Toast.LENGTH_SHORT).show()
        }
    }
}